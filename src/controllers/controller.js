const axios = require("axios");
const API_KEY = "b809d2ba6158e2cd72dfbbb63a11f712";

const Weather = require("../models/Weather");

exports.renderHomePage = (req, res) => {
    res.render("index", {
        "title": "HomePage"
    })
}

exports.getWeather = (req, res) => {
    const city = req.body.city;
    const url = `http://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${API_KEY}&units=metric`;

    const weather = new Weather(req.body.city);
    weather.validateUserInput()

    if (weather.errors.length > 0) {
        res.render("index", {
            "error" : weather.errors[0]
        })
    } else {
        axios.get(url).then((response) => {
            const {temp: temperature} = response.data.main;
            const {name: location} = response.data;
            res.render("index", {
                "weather": `${location}'s weather is ${temperature} today...`
            })
        }).catch(error => {
            console.log(error);
        });
    }
}

exports.renderAboutPage = (req, res) => {
    res.render("index")
}